if(${BOARD} STREQUAL "")
message(FATAL_ERROR "Board not specified!")
endif()


find_file(BOARD_DIR NAMES ${BOARDS_DIR}/${BOARD} NO_DEFAULT_PATH)
if(NOT ${BOARD_DIR_FOUND})
message(FATAL_ERROR "Board ${BOARD} not found!")
endif()

include(${CMAKE_CURRENT_LIST_DIR}/${BOARD}/cpuinfo.cmake)
