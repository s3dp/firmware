



add_library(
board-${BOARD} 
OBJECT
board.cpp
)


if(DEFINED REQUIRED_USB)
target_compile_definitions(board-${BOARD} PUBLIC REQUIRED_USB)
endif()
