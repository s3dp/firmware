#include <boards/board.hpp>
#include <dev/stepper/PinInterface.hpp>
#include <vector>
#include <helpers/Initable.hpp>
#include <helpers/ProceduralInitable.hpp>
#include <hal/clock.h>
#include <libopencm3/stm32/rcc.h>



#include "board.hpp"


using namespace hal;
using namespace helpers;


ProceduralInitable boardClock(std::bind(clock_use_preset, 0), true);


void initPeriphs()
{
    rcc_periph_clock_enable ( RCC_GPIOA );
    rcc_periph_clock_enable ( RCC_GPIOB );
    rcc_periph_clock_enable ( RCC_GPIOC );
    rcc_periph_clock_enable ( RCC_GPIOD );
    rcc_periph_clock_enable ( RCC_GPIOE );
    rcc_periph_clock_enable ( RCC_ADC1 );
}



ProceduralInitable boardPeriphs(initPeriphs, true);

GPIO _led(eGPIO::PE0);

GPIO xEn(eGPIO::PB1, MODE_OUT, false);
GPIO xStep(eGPIO::PB2, MODE_OUT, false);
GPIO xDir(eGPIO::PB3, MODE_OUT, false);

GPIO yEn(eGPIO::PB4, MODE_OUT, false);
GPIO yStep(eGPIO::PB5, MODE_OUT, false);
GPIO yDir(eGPIO::PB6, MODE_OUT, false);

GPIO zEn(eGPIO::PB7, MODE_OUT, false);
GPIO zStep(eGPIO::PB8, MODE_OUT, false);
GPIO zDir(eGPIO::PB9, MODE_OUT, false);

GPIO e0En(eGPIO::PB10, MODE_OUT, false);
GPIO e0Step(eGPIO::PB11, MODE_OUT, false);
GPIO e0Dir(eGPIO::PB12, MODE_OUT, false);

GPIO e1En(eGPIO::PB13, MODE_OUT, false);
GPIO e1Step(eGPIO::PB14, MODE_OUT, false);
GPIO e1Dir(eGPIO::PB15, MODE_OUT, false);

using PinIface = dev::stepper::PinInterface;

PinIface aX(&xEn, &xStep, &xDir, false);
PinIface aY(&yEn, &yStep, &yDir, false);
PinIface aZ(&zEn, &zStep, &zDir, false);
PinIface aE0(&e0En, &e0Step, &e0Dir, false);
PinIface aE1(&e1En, &e1Step, &e1Dir, false);


#ifdef REQUIRED_USB
hal::USB usb;
USB * USB::_instance = &usb;
#endif

const Initable* initables[] = {
    &boardClock,
    &boardPeriphs,
    &aX,
    &aY,
    &aZ,
    &aE0,
    &aE1,
#ifdef REQUIRED_USB
    &usb,
#endif
    &_led
};


static const boards::Board::Initializer i = {
    .initables = initables,
    .initables_size = sizeof(initables)/sizeof(Initable*),
    .led = &_led
};

const class boards::Board boards::instance(&i);



