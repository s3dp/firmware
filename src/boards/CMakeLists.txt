

find_file(BOARD_DIR NAMES ${BOARDS_DIR}/${BOARD} NO_DEFAULT_PATH)
if(NOT ${BOARD_DIR_FOUND})
message(FATAL_ERROR "Board ${BOARD} not found!")
endif()



add_subdirectory(common)
add_subdirectory(${BOARD})

add_library(
board
STATIC
$<TARGET_OBJECTS:board-common>
$<TARGET_OBJECTS:board-${BOARD}>
)
