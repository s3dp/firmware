#include <hal/usb.hpp>
#include <hal/adc.hpp>

#include <dev/stepper/PinInterface.hpp>

extern hal::USB usb;

extern dev::stepper::PinInterface aX, aY, aZ, aE0, aE1;
extern hal::ADC sensor_e0, sensor_e1, sensor_bed;
extern hal::GPIO heater_e0, heater_e1, heater_bed;
