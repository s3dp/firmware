#include <boards/board.hpp>
#include <dev/stepper/PinInterface.hpp>
#include <vector>
#include <helpers/Initable.hpp>
#include <helpers/ProceduralInitable.hpp>
#include <hal/clock.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "board.hpp"
#include <hal/usb.hpp>


using namespace hal;
using namespace helpers;


ProceduralInitable boardClock ( std::bind ( clock_use_preset, 0 ), true );


void initPeriphs()
{
    rcc_periph_clock_enable ( RCC_GPIOA );
    rcc_periph_clock_enable ( RCC_GPIOB );
    rcc_periph_clock_enable ( RCC_GPIOC );
    rcc_periph_clock_enable ( RCC_GPIOD );
    rcc_periph_clock_enable ( RCC_GPIOE );
    rcc_periph_clock_enable ( RCC_AFIO );
    rcc_periph_clock_enable ( RCC_ADC1 );
}


void initSWD()
{
    AFIO_MAPR |= AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON;
}

ProceduralInitable boardPeriphs ( initPeriphs, true );
ProceduralInitable boardSWD ( initSWD, true );


GPIO _led ( eGPIO::PC6 );

GPIO xEn ( eGPIO::PB0, MODE_OUT, false );
GPIO xStep ( eGPIO::PB1, MODE_OUT, false );
GPIO xDir ( eGPIO::PB2, MODE_OUT, false );

GPIO yEn ( eGPIO::PC0, MODE_OUT, false );
GPIO yStep ( eGPIO::PC1, MODE_OUT, false );
GPIO yDir ( eGPIO::PC2, MODE_OUT, false );

GPIO zEn ( eGPIO::PC3, MODE_OUT, false );
GPIO zStep ( eGPIO::PC4, MODE_OUT, false );
GPIO zDir ( eGPIO::PC5, MODE_OUT, false );

GPIO e0En ( eGPIO::PC13, MODE_OUT, false );
GPIO e0Step ( eGPIO::PC14, MODE_OUT, false );
GPIO e0Dir ( eGPIO::PC15, MODE_OUT, false );

GPIO e1En ( eGPIO::PE11, MODE_OUT, false );
GPIO e1Step ( eGPIO::PE12, MODE_OUT, false );
GPIO e1Dir ( eGPIO::PE13, MODE_OUT, false );

GPIO sensor_e0_pin(eGPIO::PA0, MODE_ANAL, false);
GPIO sensor_e1_pin(eGPIO::PA1, MODE_ANAL, false);
GPIO sensor_bed_pin(eGPIO::PA2, MODE_ANAL, false);

ADC sensor_e0(&sensor_e0_pin, 0, 0);
ADC sensor_e1(&sensor_e1_pin, 0, 1);
ADC sensor_bed(&sensor_bed_pin, 0, 2);

GPIO heater_e0(eGPIO::PA15, MODE_OUT, false);
GPIO heater_e1(eGPIO::PB3, MODE_OUT, false);
GPIO heater_bed(eGPIO::PB10, MODE_OUT, false);

using PinIface = dev::stepper::PinInterface;

PinIface aX ( &xEn, &xStep, &xDir, false );
PinIface aY ( &yEn, &yStep, &yDir, false );
PinIface aZ ( &zEn, &zStep, &zDir, false );
PinIface aE0 ( &e0En, &e0Step, &e0Dir, false );
PinIface aE1 ( &e1En, &e1Step, &e1Dir, false );

#ifdef REQUIRED_USB
hal::USB usb;
USB * USB::_instance = &usb;
#endif

const Initable* initables[] = {
    &boardClock,
    &boardPeriphs,
    &boardSWD,
    &aX,
    &aY,
    &aZ,
    &aE0,
    &aE1,
    &_led,
#ifdef REQUIRED_USB
    &usb,
#endif

};


static const boards::Board::Initializer i = {
    .initables = initables,
    .initables_size = sizeof ( initables ) /sizeof ( Initable* ),
    .led = &_led
};

const class boards::Board boards::instance ( &i );




