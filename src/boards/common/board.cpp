#include <boards/board.hpp>


namespace boards{
    




Board::Board(const boards::Board::initializer_t* initializer):
Initable(true),
_initializer(initializer)
{
    
}

Board::~Board(){
    
}

bool Board::sanityCheck(){
    return true;
}

void Board::_init()
{
    if((!_initializer)||(!_initializer->initables)){
        return;
    }
    for(size_t i = 0; i<_initializer->initables_size; ++i){
        _initializer->initables[i]->init();
    }
}



const hal::GPIO * Board::led() const
{
    return (_initializer!=nullptr)?_initializer->led:nullptr;
}





}
