#include "tasks.hpp"
#include <string>
#include <cstring>

#include <printer/Printer.hpp>

#include <mod/gcode/CommandFactory.hpp>

using namespace hal;

const static size_t BUF_MAX = 256;

const char * MS_OK = "echo:ok\n";
const char * MS_FAIL = "echo::fail\n";


static void _run(const std::string& what, Serial * from){
    const char * retms;
    mod::gcode::Command cmd;    
    strcpy(cmd.rawCommand, what.data());
    cmd.source = from;
    auto cfInst = mod::gcode::CommandFactory::getInstance();
    if(cfInst&&cfInst->parse(cmd)){
       cmd.handler(cmd);
    } else {
        retms = MS_FAIL;
    }    
    
    from->write(retms,strlen(retms));
}


static bool _poll(const Serial * serial){
    
    return const_cast<Serial *>(serial)->poll(Serial::STATUS_IDATA)&Serial::STATUS_IDATA;
}



static void _readSerial(Serial * serial, std::string& where){
    char c;
    for(;;){        
        if(serial->read(&c, 1)){
            where += c;
            if(c == '\n' || c == '\r' || where.size()>=BUF_MAX){
                break;
            }
        }
        _poll(serial);
        
    }
    serial->flush();
}

void TaskCommand(void * args __attribute((unused))){
    auto printer = printer::Printer::getInstance();
    auto& serials = printer->modules->serials;
    
    for(;;){
        size_t i = 0;
        std::string buf;
        while(serials[i]){
            auto serial = serials[i];
            if(_poll(serial)){
                // If we have any data from any serial we should get whole command and run it
                _readSerial(const_cast<hal::Serial *>(serial), buf);
                _run(buf, const_cast<hal::Serial *>(serial));
                buf.clear();
            }            
            ++i;
            
        }
        
    }
}
