#include "tasks.hpp"


#include <cstdint>
#include <time.h>


#include <hal/gpio.hpp>
#include <hal/adc.hpp>
#include <printer/Printer.hpp>
#include <mod/thermal/Regulator.hpp>
#include <helpers/exception.hpp>

#include <FreeRTOS.h>
#include <task.h>
#include <projdefs.h>


void TaskADC(void * args __attribute((unused))){
    auto pInst = printer::Printer::getInstance();
    if(pInst == nullptr||pInst->modules == nullptr){
        helpers::fatal_error();
    }
    auto hotends = pInst->modules->hotends;
    auto table = pInst->modules->table;
    const int32_t STARTUP_TEMP = mod::thermal::Regulator::TEMP_DISABLE;
    if(hotends){
            for(size_t i = 0; hotends[i]; ++i){               
                const_cast<mod::thermal::Regulator *>(hotends[i])->setTemperature(STARTUP_TEMP);;
            }
        }
    if(table){
        const_cast<mod::thermal::Regulator *>(table)->setTemperature(STARTUP_TEMP);;
    }
    
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = pdMS_TO_TICKS(100);

     // Initialise the xLastWakeTime variable with the current time.
    xLastWakeTime = xTaskGetTickCount();

     
    for(;;){
        vTaskDelayUntil( &xLastWakeTime, xFrequency );
        if(hotends){
            for(size_t i = 0; hotends[i]; ++i){               
                const_cast<mod::thermal::Regulator *>(hotends[i])->tick(100);
            }
        }
        if(table){
            const_cast<mod::thermal::Regulator *>(table)->tick(100);
        }
        xLastWakeTime = xTaskGetTickCount();
        
        
    }
}
