#include "tasks.hpp"

#include <cmath>
#include <cstdint>

#include <boards/board.hpp>

#include <FreeRTOS.h>
#include <task.h>

uint32_t ledblink(int a){
    float v = a / (10*3.14159);
    float sinV = sin(v);
    uint32_t t = 40.0*abs(sinV);
    auto led = boards::instance.led();
    if(led){
        led->toggle();
    }
    return t;
    
}

void TaskLed(void * args __attribute((unused))){
    int i = 0;
    for(;;){
        ledblink(++i);
        vTaskDelay(100);
        if(i>=65535)i = 0;
        
    }
}




