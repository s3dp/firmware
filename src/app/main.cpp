#include <math.h>
#include <printer/Printer.hpp>

#include <hal/gpio.hpp>
#include <hal/adc.hpp>


#include <FreeRTOS.h>
#include <task.h>


#include "tasks.hpp"






int main()
{
    using namespace printer;
    auto p = Printer::getInstance();
    p->init(true);
    const char args[] = {};
    xTaskCreate(TaskLed,"LED",2048,(void*)args,configMAX_PRIORITIES-1,0);
    xTaskCreate(TaskADC,"ADC",1024,(void*)args,configMAX_PRIORITIES-1,0);
    xTaskCreate(TaskCommand,"CMD",4096,(void*)args,configMAX_PRIORITIES-1,0);


    vTaskStartScheduler();
    
    for(;;);

}





