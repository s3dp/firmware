

#include <freertos/Tasks.hpp>
// clang-format off
#include "FreeRTOS.h"

#include <task.h>
// clang-format on

namespace freertos {

// xTaskCreate(TaskLed,"LED",2048,(void*)args,configMAX_PRIORITIES-1,0);
void * taskCreate(task_fn_t fn, const char * name)
{
    TaskHandle_t thandle;
    xTaskCreate(fn, name, 512, (void *){}, configMAX_PRIORITIES - 1, &thandle);
    return (void *)thandle;
}

// TODO: fix
void taskKill(void * pid)
{
    if (pid) {
        vTaskDelete((TaskHandle_t)pid);
    }
}


void taskSuspend(void * pid)
{
    if (pid) {
        vTaskSuspend((TaskHandle_t)pid);
    }
}

void taskResume(void * pid)
{
    if (pid) {
        vTaskResume((TaskHandle_t)pid);
    }
}

namespace{
inline uint32_t msToTicks(uint32_t ms){
    return pdMS_TO_TICKS(ms);
}


inline uint32_t ticksToMs(uint32_t ticks){
    return (ticks*1000)/configTICK_RATE_HZ;
}
}
void taskDelay(uint32_t ms){
    vTaskDelay(pdMS_TO_TICKS(ms));
}

void taskDelayUntil(uint32_t& last_ms, uint32_t ms){
    uint32_t lastTicks = pdMS_TO_TICKS(last_ms);
    vTaskDelayUntil(&lastTicks, pdMS_TO_TICKS(ms));
    last_ms = ticksToMs(lastTicks);
}

uint32_t time(){
    return ticksToMs(xTaskGetTickCount());
}
}  // namespace freertos
