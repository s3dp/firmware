#include <FreeRTOS.h>
#include <task.h>

#include <libopencm3/stm32/rcc.h>


uint32_t getCurrentFrequency(void);


void vApplicationIdleHook( void )
{
    for( ;; );
}

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
    /* This function will get called if a task overflows its stack.   If the
    parameters are corrupt then inspect pxCurrentTCB to find which was the
    offending task. */

    ( void ) pxTask;
    ( void ) pcTaskName;

    for( ;; );
}



void cassert(uint32_t file, const char * line){
    (void)file;
    (void)line;
    
    for(;;);
    
}


uint32_t getCurrentFrequency(){
    return rcc_ahb_frequency;
}
