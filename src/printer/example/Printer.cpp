#include <board.hpp>
#include <boards/board.hpp>
#include <dev/thermal/HeaterPushPull.hpp>
#include <dev/thermal/SensorAnalogLinear.hpp>
#include <mod/thermal/RegulatorPID.hpp>
#include <mod/thermal/RegulatorPushPull.hpp>
#include <printer/Printer.hpp>

using namespace printer;

const hal::Serial * serials[] = {&usb, nullptr};

const dev::stepper::Stepper * steppers[] = {nullptr};

using HeaterPP = dev::thermal::HeaterPushPull;
using dev::thermal::Heater;
using SensorAnal   = dev::thermal::SensorAnalogLinear;
using RegulatorPP  = mod::thermal::RegulatorPushPull;
using RegulatorPID = mod::thermal::RegulatorPID;
using ConfigPID    = RegulatorPID::Config;

auto sensorPreset = &dev::thermal::NTC100K_4K7_PULLUP;

HeaterPP bedHeater(&heater_bed);
HeaterPP hotendHeater(&heater_e0);
HeaterPP hotend2Heater(&heater_e1);

SensorAnal bedSensor(&sensor_bed, sensorPreset);
SensorAnal hotendSensor(&sensor_e0, sensorPreset);
SensorAnal hotend2Sensor(&sensor_e1, sensorPreset);

RegulatorPP bedRegulator(&bedSensor, static_cast<Heater *>(&bedHeater));
RegulatorPP hotendRegulator(&hotendSensor, &hotendHeater);

const ConfigPID h2pidConfig = {
    .sensor = &hotend2Sensor,
    .heater = &hotend2Heater,
    .kP = 200,
    .kD = 200,
    .kI = 200,
};
RegulatorPID hotend2Regulator(&h2pidConfig);

const mod::thermal::Regulator * hotends[] = {&hotendRegulator, &hotend2Regulator, nullptr};

const static Printer::init_t initializer = {.board = &boards::instance,
    .steppers                                      = steppers,
    .serials                                       = serials,
    .hotends                                       = hotends,
    .table                                         = &bedRegulator};

Printer P(&initializer);

const Printer * Printer::_instance = &P;
