#include <printer/Printer.hpp>
#include <helpers/exception.hpp>
// #include <stdexcept>


namespace printer{
Printer::Printer(const init_t * _modules):
Initable(true),
modules(_modules)
{
    if(
        modules==nullptr 
        || modules->board == nullptr 
        || modules->serials == nullptr 
        || modules->steppers == nullptr
    ){
        helpers::fatal_error();
    }
}



void Printer::_init()
{
    modules->board->init(true);
    modules->board->led()->set();
    auto& serials = modules->serials;
    size_t i = 0;
    while(serials[i]){
        serials[i]->init(true);
        ++i;
    }
    
    auto& steppers = modules->steppers;
    i = 0;
    while(steppers[i]){
        steppers[i]->init(true);
        ++i;
    }
    
    auto& hotends = modules->hotends;
    for(i = 0; hotends[i]; ++i){
        const_cast<regulator_t *>(hotends[i])->init(true);
    }
    
    const_cast<regulator_t *>(modules->table)->init(true);
    
    
    modules->board->led()->reset();
}

Printer * Printer::getInstance()
{
    return const_cast<Printer*>(_instance);
}





}
