#include <helpers/Initable.hpp>



namespace helpers{
    
Initable::Initable(bool required): 
_required(required),
_inited(false)
{

}    

void Initable::init(bool force) const{
    if((force||_required)&&(!_inited)){
        const_cast<Initable *>(this)->_init();
        *(const_cast<bool *>(&_inited)) = true;
    }
}
}
