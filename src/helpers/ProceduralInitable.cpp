
#include <helpers/ProceduralInitable.hpp>


namespace helpers{
ProceduralInitable::ProceduralInitable(const ProceduralInitable::fn_t& fn, bool force):
Initable(force),
_fn(fn)
{
    
}

void ProceduralInitable::_init(){
    _fn();
}
    
}

