

#include <mod/thermal/RegulatorPushPull.hpp>


#include <helpers/exception.hpp>

namespace mod {
namespace thermal {

RegulatorPushPull::RegulatorPushPull(const Sensor * sensor, const Heater * heater) :
    _sensor(const_cast<Sensor *>(sensor)),
    _heater(const_cast<Heater *>(heater)),
    _target_temp(TEMP_DISABLE)
{
    if(_sensor==nullptr||_heater==nullptr){
        helpers::fatal_error();
    }
    
}

void RegulatorPushPull::setTemperature(int32_t temp)
{
    _target_temp = temp;
}

int32_t RegulatorPushPull::getMeasuredTemperature() const
{
    return _sensor->measure();
}

int32_t RegulatorPushPull::getTargedTemperature() const
{
    return _target_temp;
}

void RegulatorPushPull::tick(uint32_t dt)
{
    int32_t temp       = _sensor->measure();
    bool _heater_state = _heater->getPower();
    if (_heater_state) {
        if (temp > (_target_temp + _HYST)) {
            _heater->setPower(0);
        } else {
            _heater->setPower(1);
        }
    } else {
        if (temp <= _target_temp) {
            _heater->setPower(1);
        } else {
            _heater->setPower(0);
        }
    }
}

void RegulatorPushPull::_init(){
    _heater->init(true);
    _sensor->init(true);
};

}  // namespace thermal
}  // namespace modsetPower(_state);
