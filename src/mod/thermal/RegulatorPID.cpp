
#include <helpers/math.h>

#include <helpers/exception.hpp>
#include <mod/thermal/RegulatorPID.hpp>

namespace mod {
namespace thermal {

RegulatorPID::RegulatorPID(const Config * cfg) :
    _cfg(cfg),
    _target_temp(TEMP_DISABLE),
    _tempPrev(0),
    vP(0),
    vD(0),
    vI(0)
{
    if (cfg == nullptr || cfg->sensor == nullptr || cfg->heater == nullptr) {
        helpers::fatal_error();
    }
}

void RegulatorPID::setTemperature(int32_t temp)
{
    _target_temp = temp;
}

int32_t RegulatorPID::getMeasuredTemperature() const
{
    return _cfg->sensor->measure();
}

int32_t RegulatorPID::getTargedTemperature() const
{
    return _target_temp;
}

namespace {
static const int32_t KI_MAX = 32767;
int32_t evalVd(int32_t last, int32_t tempCurrent, int32_t tempPrev, int32_t Kd)
{
    float diffVd = Kd * (tempPrev - tempCurrent) - last;
    return last + 0.95*diffVd;
}

int32_t evalVi(int32_t last, int32_t tempCurrent, int32_t tempPrev, int32_t Ki)
{
    int32_t Vi = Ki * (last - tempCurrent + tempPrev);
    return helpers::toBounds(Vi, -KI_MAX, KI_MAX);
    
}

int32_t evalVp(int32_t tempCurrent, int32_t tempTarget, int32_t Kp)
{
    return Kp * (tempCurrent - tempTarget);
}
}  // namespace

void RegulatorPID::tick(uint32_t dt)
{
    int32_t temp       = _cfg->sensor->measure();
    if (_target_temp > TEMP_DISABLE) {
        // We should control temperature
        int32_t err = _target_temp - temp;
        if (helpers::inBounds(err, _target_temp - PID_MAX_DELTA, _target_temp + PID_MAX_DELTA)) {
            // We should operate with PID
            vD = evalVd(vD, temp, _tempPrev, _cfg->kD);
            vI = evalVi(vI, temp, _tempPrev, _cfg->kI);
            vP = evalVp(temp, _target_temp, _cfg->kP);
            int32_t pwr = vD + vI + vP;
            pwr = helpers::toBounds(pwr, (int32_t)Heater::PWR_NONE, (int32_t)Heater::PWR_MAX);
            _cfg->heater->setPower(pwr);
        } else {
            // We should operate without PID
            vP = 0;
            vD = 0;
            vI = 0;
            if (temp > _target_temp) {
                _cfg->heater->setPower(Heater::PWR_NONE);
            } else {
                _cfg->heater->setPower(Heater::PWR_MAX);
            }
        }

    } else {
        // Heater should be disabled anyway
        _cfg->heater->setPower(Heater::PWR_NONE);
    }
}

void RegulatorPID::_init()
{
    _cfg->heater->init(true);
    _cfg->sensor->init(true);
};

}  // namespace thermal
}  // namespace mod
