#include <mod/gcode/CommandFactory.hpp>

#include "MCommands.hpp"

namespace mod {
namespace gcode {

namespace {

const CommandFactory cmdFactoryInst;

}

const CommandFactory * CommandFactory::_instance = &cmdFactoryInst;

const CommandFactory * CommandFactory::getInstance()
{
    return _instance;
}


void CommandFactory::_parseArgs(Command & cmd) const
{
    auto istr = cmd.rawCommand;
    cmd.argc  = 0;
    if (!istr) {
        return;
    }

    char * abegin = istr;
    size_t asize  = 0;
    size_t i;
    for (i = 0; istr[i]; ++i) {
        char c = istr[i];
        if (c == ';' || c == '\n' || c == '\r') {
            break;
        }
        if (isspace(c)) {
            if (asize) {
                if (cmd.argc >= 9) {
                    return;
                }
                cmd.argv[cmd.argc] = abegin;
                istr[i]            = '\0';
                ++cmd.argc;
                asize = 0;
            }
            if (istr[i + 1]) {
                abegin = &istr[i + 1];
                asize  = 0;
            }

            continue;
        }
        ++asize;
    }
    if (asize) {
        if (cmd.argc >= 9) {
            return;
        }
        cmd.argv[cmd.argc] = abegin;
        istr[i]            = '\0';
        ++cmd.argc;
        asize = 0;
    }
}

const CommandPrimitive * CommandFactory::_find(const char * cmd) const
{
    uint16_t index = std::atoi(&cmd[1]);
    for(size_t i = 0; i<_nCommands; ++i){
        auto& found = _knownCommands[i];
        if(found.set==cmd[0]&&found.index==index){
            return &_knownCommands[i];
        }
    }
    return nullptr;
}

bool CommandFactory::parse(Command & cmd) const
{
    _parseArgs(cmd);
    if (!cmd.argc) {
        return false;
    }
    const char * argv = cmd.argv[0];
    if(argv[0]=='N'){
        argv = cmd.argv[1];
    }
    auto primitivePtr = _find(argv);
    if (primitivePtr) {
        cmd.handler = primitivePtr->handler;
        cmd.set     = primitivePtr->set;
        cmd.index   = primitivePtr->index;
    }
    return primitivePtr != nullptr;
}

const CommandPrimitive CommandFactory::_knownCommands[] = {    
    {.set = 'M', .index = 104, .handler = handlerM104},
    {.set = 'M', .index = 105, .handler = handlerM105},    
    {.set = 'M', .index = 110, .handler = handlerM110},
    {.set = 'M', .index = 115, .handler = handlerM115},
    {.set = 'M', .index = 140, .handler = handlerM140},
    {.set = 'M', .index = 155, .handler = handlerM155},
};

const size_t CommandFactory::_nCommands =
    sizeof(CommandFactory::_knownCommands) / sizeof(CommandPrimitive);

}  // namespace gcode
}  // namespace mod
