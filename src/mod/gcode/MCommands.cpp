#include "MCommands.hpp"

#include <libopencm3/stm32/desig.h>

#include <cstring>
#include <freertos/Tasks.hpp>
#include <mod/thermal/Regulator.hpp>
#include <printer/Printer.hpp>

namespace mod {
namespace gcode {
    
void handlerM110(const Command & cmd)
{
    const char message[] = "echo:ok\n\0";
    
    cmd.source->write(message, strlen(message));
}

void handlerM115(const Command & cmd)
{
    char message[100];
    uint32_t id[3];
    desig_get_unique_id(id);
    sprintf(message, "FIRMWARE_NAME:S3DP-FIRMWARE 0.0.0 (Gitlab) ");
    cmd.source->write(message, strlen(message));
    sprintf(message, "SOURCE_CODE_URL:https://gitlab.com/s3dp/firmware ");
    cmd.source->write(message, strlen(message));
    sprintf(message, "PROTOCOL_VERSION:0.0 MACHINE_TYPE:RepRap EXTRUDER_COUNT:1 ");
    cmd.source->write(message, strlen(message));
    sprintf(message, "UUID: %08lx-%08lx-%08lx\necho:ok\n", id[0], id[1], id[2]);
    cmd.source->write(message, strlen(message));
}

/**
 * Set hotend temperature
 *
 * M104 [B<temp>] [F<flag>] [S<temp>] [T<index>]
 * B - max temp
 * S - target temp
 * T - target extruder
 */
void handlerM104(const Command & cmd)
{
    using namespace mod::thermal;
    char message[100];
    bool doWork  = false;
    int32_t temp = Regulator::TEMP_DISABLE;
    size_t e_id  = 0;

    for (size_t i = 1; i < cmd.argc; ++i) {
        auto arg = cmd.argv[i];
        switch (arg[0]) {
            case 'B': {
                break;
            }
            case 'S': {
                doWork        = true;
                float tmpTemp = std::atof(&arg[1]);
                if (tmpTemp <= 0.1) {
                    temp = Regulator::TEMP_DISABLE;
                } else {
                    temp = (tmpTemp * 100.0);
                }
                break;
            }
            case 'T': {
                e_id = std::atoi(&arg[1]);
                break;
            }
            default: {
                break;
            }
        }
    }

    if (!doWork) {
        sprintf(message, "echo:ok\n");
        cmd.source->write(message, strlen(message));
        return;
    }
    auto pInst = printer::Printer::getInstance();
    if (!pInst) {
        sprintf(message, "echo:fail ; printer not found\n");
        cmd.source->write(message, strlen(message));
        return;
    }
    auto hotends = pInst->modules->hotends;
    for (size_t i = 0; i < e_id; ++i) {
        if (hotends[i] == 0) {
            sprintf(message, "echo:fail ; extruder not found\n");
            cmd.source->write(message, strlen(message));
            return;
        }
    }
    const_cast<printer::Printer::regulator_t *>(hotends[e_id])->setTemperature(temp);
    sprintf(message, "echo:ok ; extruder %u temp will be %d.%d\n", (unsigned short)e_id,
        (short)temp / 100, (short)temp % 100);
    cmd.source->write(message, strlen(message));
}

/**
 * Set bed temperature
 *
 * M140 [S<temp>]
 * S - target temp
 */
void handlerM140(const Command & cmd)
{
    using namespace mod::thermal;
    char message[100];
    bool doWork  = false;
    int32_t temp = Regulator::TEMP_DISABLE;

    for (size_t i = 1; i < cmd.argc; ++i) {
        auto arg = cmd.argv[i];
        switch (arg[0]) {
            case 'S': {
                doWork        = true;
                float tmpTemp = std::atof(&arg[1]);
                if (tmpTemp <= 0.1) {
                    temp = Regulator::TEMP_DISABLE;
                } else {
                    temp = (tmpTemp * 100.0);
                }
                break;
            }

            default: {
                break;
            }
        }
    }

    if (!doWork) {
        sprintf(message, "echo:ok ; I will do nothing\n");
        cmd.source->write(message, strlen(message));
        return;
    }
    auto pInst = printer::Printer::getInstance();
    if (!pInst) {
        sprintf(message, "echo:fail ; printer not found\n");
        cmd.source->write(message, strlen(message));
        return;
    }
    auto table = pInst->modules->table;
    if (table == 0) {
        sprintf(message, "echo:fail ; table not found\n");
        cmd.source->write(message, strlen(message));
        return;
    }
    const_cast<printer::Printer::regulator_t *>(table)->setTemperature(temp);
    sprintf(message, "echo:ok ; table temp will be %d.%d\n", (short)temp / 100, (short)temp % 100);
    cmd.source->write(message, strlen(message));
}

namespace {
using serial_t = printer::Printer::serial_t;
void tReport(serial_t & serial)
{
    char message[100];
    auto pInst = printer::Printer::getInstance();
    if (!pInst) {
        sprintf(message, "echo:fail ; printer not found\n");
        serial.write(message, strlen(message));
        return;
    }
    auto table = pInst->modules->table;
    if (table) {
        auto tTemp = table->getMeasuredTemperature();
        auto aTemp = table->getTargedTemperature();
        sprintf(message, "B: %d.%02d(%d.%02d); ", (short)tTemp / 100, (short)tTemp % 100,
            (short)aTemp / 100, (short)aTemp % 100);
        serial.write(message, strlen(message));
    }
    auto hotends = pInst->modules->hotends;
    for (size_t i = 0; hotends[i]; ++i) {
        auto tTemp = hotends[i]->getMeasuredTemperature();
        auto aTemp = hotends[i]->getTargedTemperature();
        sprintf(message, "E%u: %d.%02d(%d.%02d); ", (unsigned short)i, (short)tTemp / 100,
            (short)tTemp % 100, (short)aTemp / 100, (short)aTemp % 100);
        serial.write(message, strlen(message));
    }
    serial.write("\n", 1);
}
}  // namespace

/**
 * Report temperature
 *
 * M105 [T<index>]
 */
void handlerM105(const Command & cmd)
{
    using namespace mod::thermal;
    char message[100];

    tReport(*cmd.source);

    sprintf(message, "\necho:ok\n");
    cmd.source->write(message, strlen(message));
}

namespace {

bool _trptSemaphore  = false;
void * _pTaskTreport = nullptr;
uint32_t _trptLastTime;
uint32_t _trptDelayMS;
serial_t * _trptSerial;
void fnTaskTReport(void * args __attribute((unused)))
{
    _trptLastTime = freertos::time();
    for (;;) {
        freertos::taskDelayUntil(_trptLastTime, _trptDelayMS);
        _trptSemaphore = true;
        if (_trptSerial) {
            tReport(*_trptSerial);
        }
        _trptSemaphore = false;
    }
}
}  // namespace

/**
 * Temperature autoreport
 *
 * M155 [S<seconds>]
 */
void handlerM155(const Command & cmd)
{
    using namespace mod::thermal;
    char message[100];
    uint32_t ms = 0;

    for (size_t i = 1; i < cmd.argc; ++i) {
        auto arg = cmd.argv[i];
        switch (arg[0]) {
            case 'S': {
                float tmpSec = std::atof(&arg[1]);
                if (tmpSec <= 0.1) {
                    ms = 0;
                } else {
                    ms = (tmpSec * 1000.0);
                }
                break;
            }
            default: {
                break;
            }
        }
    }
    while (_trptSemaphore)
        ;
    freertos::taskSuspend(_pTaskTreport);
    _trptDelayMS = ms;
    _trptSerial  = cmd.source;

    sprintf(message, "\necho:ok\n");
    cmd.source->write(message, strlen(message));

    if (ms) {
        if (!_pTaskTreport) {
            _pTaskTreport = freertos::taskCreate(fnTaskTReport, "TRPT");
        } else {
            _trptLastTime = freertos::time();
            freertos::taskResume(_pTaskTreport);
        }
    }
}

}  // namespace gcode
}  // namespace mod
