

add_library(
    mod_gcode 
    STATIC 
    CommandFactory.cpp
    MCommands.cpp
)

target_link_libraries(
mod_gcode 
freertos
${LIBOPENCM3_LIBS} # libopencm3
)


