#pragma once
#include <mod/gcode/Command.hpp>
namespace mod {
namespace gcode {
void handlerM104(const Command & cmd);
void handlerM105(const Command & cmd);
void handlerM110(const Command & cmd);
void handlerM115(const Command & cmd);
void handlerM140(const Command & cmd);
void handlerM155(const Command & cmd);
}
}  // namespace mod
