#include <dev/stepper/PinInterface.hpp>




namespace dev {
namespace stepper {
PinInterface::PinInterface(const hal::GPIO * en, const hal::GPIO * step, const hal::GPIO * dir, const bool doInit):
    Initable(doInit),
    _en(en),
    _step(step),
    _dir(dir)    
{


}

PinInterface::~PinInterface()
{
    
}

void PinInterface::_init()
{
    _en->init(true);
    _step->init(true);
    _dir->init(true);
    
}


}
}
