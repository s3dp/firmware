

#include <dev/thermal/HeaterPushPull.hpp>

#include <helpers/exception.hpp>

namespace dev {
namespace thermal {

HeaterPushPull::HeaterPushPull(hal::GPIO * pin) : _pin(pin), _state(false)
{
    if(_pin==nullptr){
        helpers::fatal_error();
    }
}

void HeaterPushPull::setPower(uint16_t power)
{
    _state = power != 0;
    _pin->write(_state);
}

uint16_t HeaterPushPull::getPower() const
{
    return _state;
}

HeaterPushPull::~HeaterPushPull()
{
    _pin->reset();
}

void HeaterPushPull::_init()
{
    _pin->init(true);
    setPower(_state);
}

}  // namespace thermal
}  // namespace dev
