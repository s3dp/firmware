

#include <dev/thermal/SensorAnalogLinear.hpp>
#include <helpers/exception.hpp>

#include <cmath>

namespace dev {
namespace thermal {

// clang-format off
LinearSensorPreset NTC100K_4K7_PULLUP = {
    .temperature_initial = 2500, 
    .resistance_initial = 100000, 
    .b_value = 4066
};

LinearSensorPreset NTC100K_1K_PULLUP = {
    .temperature_initial = 2500, 
    .resistance_initial = 100000, 
    .b_value = 4066,
    .pullup_resistance = 1000
};
// clang-format on

SensorAnalogLinear::SensorAnalogLinear(hal::ADC * adc, LinearSensorPreset * preset) :
    _adc(adc),
    _preset(preset)
{
    if(_adc==nullptr||_preset==nullptr){
        helpers::fatal_error();
    }
}



int32_t SensorAnalogLinear::measure(){
    int32_t adcValue = _adc->getMeasuredValue();
    // to resistance
    // (vPullup + vNTC) = 65535
    // rNTC = vNTC*rPullup/vPullup
    // I = vPullup/rPullup
    // vPullup = 65535-vNTC
    float rNTC = (adcValue * _preset->pullup_resistance / (65535 - adcValue));
    float ret = rNTC / _preset->resistance_initial;
    /*
     * 1/T = 1/T0 + (1/Tb)ln(R/R0)
     * T is deg Kelvin
     */
    ret = log(ret);
    ret /= _preset->b_value;
    ret += 1.0/(_preset->temperature_initial/100 + 273.15);
    ret =  1.0/ret;
    ret *= 100;
    ret -= 27315;
    
    return ret;
    
}



SensorAnalogLinear::~SensorAnalogLinear()
{
}

void SensorAnalogLinear::_init()
{
    _adc->init(true);
}

}  // namespace thermal
}  // namespace dev
