#include <hal/clock.h>
#include <libopencm3/stm32/rcc.h>
#include <stddef.h>




static const void (*_presets[])() = {
    rcc_clock_setup_in_hse_8mhz_out_72mhz
};

static const size_t _presetsLen = sizeof(_presets)/sizeof(void(*)());

void clock_use_preset(uint32_t n){
    if(n>=_presetsLen){
        n = 0;
    }
    _presets[n]();   
        
}

