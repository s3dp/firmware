#include <hal/adc.hpp>
#include <libopencm3/stm32/adc.h>
#include <helpers/exception.hpp>

namespace hal{
    
    
static const uint32_t _ADCs[] = {
    ADC1,
    ADC2,
    ADC3
};


static const uint8_t _nADCs = sizeof(_ADCs)/sizeof(uint32_t);
static const uint8_t _maxChannel = 15;


ADC::ADC(const GPIO * pin, uint8_t device, uint8_t channel, uint16_t vRef, bool doInit):
Initable(doInit),
_vRef(vRef),
_channel(channel),
_dev(device),
_pin(pin)
{
    if(_dev>=_nADCs||_channel>_maxChannel||(!_pin)){
        helpers::fatal_error();
    }
}

void ADC::_init()
{
    _pin->init(true);
    uint32_t adc = _ADCs[_dev];
    adc_power_off(adc);

	/* We configure everything for one single conversion. */
	adc_disable_scan_mode(adc);
	adc_set_single_conversion_mode(adc);
	adc_disable_external_trigger_regular(adc);
	adc_set_right_aligned(adc);
	adc_set_sample_time_on_all_channels(adc, ADC_SMPR_SMP_28DOT5CYC);

	adc_power_on(adc);

	/* Wait for ADC starting up. */
	int i;
	for (i = 0; i < 800000; i++) /* Wait a bit. */
		__asm__("nop");

	adc_reset_calibration(adc);
	adc_calibrate(adc);
}


uint16_t ADC::_measure() const
{
    uint8_t channel = _channel;
    uint32_t adc = _ADCs[_dev];
	adc_set_regular_sequence(adc, 1, &channel);
	adc_start_conversion_direct(adc);
	while (!adc_eoc(adc));
	uint16_t reg16 = adc_read_regular(adc);
	return reg16;
}

uint16_t ADC::_convert(uint16_t what) const
{
    uint32_t ret;
    ret = what<<4;
    ret = (ret*_vRef)/65535;
//     ret = (_vRef==0)?(0):(ret/vDiv);
    return ret;
}


uint16_t ADC::getMeasuredValue() const
{
    return _measure()<<4;
}


uint16_t ADC::getMeasuredVoltage() const
{
    return _convert(_measure());
}


}

