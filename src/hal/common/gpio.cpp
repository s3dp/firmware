#include <hal/gpio.hpp>
#include <libopencm3/stm32/gpio.h>

namespace hal{

const uint32_t GPIO::_PortBinds[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE,
    GPIOF,
    GPIOG
};
}






