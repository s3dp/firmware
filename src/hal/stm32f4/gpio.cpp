#include <hal/gpio.hpp>


#include <libopencm3/stm32/gpio.h>



namespace hal {


GPIO::GPIO(const eGPIO portpin, const GPIOMode mode, const bool doInit):
    Initable(doInit),
    _port(GPIO::_PortBinds[portpin>>16]),
    _pin(portpin&0xffff),    
    _mode(mode)
{

}



void GPIO::toggle() const {
    gpio_toggle(_port, _pin);
}

void GPIO::write(bool value) const {
    if(value) {
        set();
    } else {
        reset();
    }
}
void GPIO::set() const {
    gpio_set(_port, _pin);
}
void GPIO::reset() const {
    gpio_clear(_port, _pin);
}
bool GPIO::read() const
{
    return gpio_get(_port, _pin);
}

uint32_t modeByEnum(GPIOMode m){
    switch(m){
        case MODE_AF:
            return GPIO_MODE_AF;
        case MODE_ANAL:
            return GPIO_MODE_ANALOG;
        case MODE_IN:
            return GPIO_MODE_INPUT;
        case MODE_OUT:
            return GPIO_MODE_OUTPUT;
    }
    return 0;
}


void GPIO::_init() {
    gpio_mode_setup(_port, modeByEnum(_mode), GPIO_PUPD_NONE, _pin);
}


}

