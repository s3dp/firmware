#include <hal/adc.hpp>
#include <libopencm3/stm32/adc.h>
#include <helpers/exception.hpp>

namespace hal{
    
    
static const uint32_t _ADCs[] = {
    ADC1,
    ADC2,
    ADC3
};


static const uint8_t _nADCs = sizeof(_ADCs)/sizeof(uint32_t);
static const uint8_t _maxChannel = 15;


ADC::ADC(const GPIO * pin, uint8_t device, uint8_t channel, uint16_t vRef, bool doInit):
Initable(doInit),
_vRef(vRef),
_channel(channel),
_dev(device),
_pin(pin)
{
    if(_dev>=_nADCs||_channel>_maxChannel||(!_pin)){
        helpers::fatal_error();
    }
}

void ADC::_init()
{
    _pin->init(true);
    uint32_t adc = _ADCs[_dev];
    adc_power_off(adc);
	adc_disable_scan_mode(adc);
	adc_set_sample_time_on_all_channels(adc, ADC_SMPR_SMP_3CYC);

	adc_power_on(adc);
}


uint16_t ADC::_measure()
{
    uint8_t channel_array[16];
	channel_array[0] = _channel;
    uint32_t adc = _ADCs[_dev];
	adc_set_regular_sequence(adc, 1, channel_array);
	adc_start_conversion_regular(adc);
	while (!adc_eoc(adc));
	uint16_t reg16 = adc_read_regular(adc);
	return reg16;
}

uint16_t ADC::_convert(uint16_t what)
{
    uint32_t ret;
    ret = what<<4;
    ret = (ret*_vRef)/65535;
//     ret = (_vRef==0)?(0):(ret/vDiv);
    return ret;
}


uint16_t ADC::getMeasuredValue()
{
    return _measure()<<4;
}


uint16_t ADC::getMeasuredVoltage()
{
    return _convert(_measure());
}


}
