#include <hal/clock.h>
#include <libopencm3/stm32/rcc.h>
#include <stddef.h>

static const struct rcc_clock_scale * _presets[] = {
    &rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]
};

static const size_t _presetsLen = sizeof(_presets)/sizeof(struct rcc_clock_scale *);

void clock_use_preset(uint32_t n){
    if(n>=_presetsLen){
        n = 0;
    }
    rcc_clock_setup_pll(_presets[n]);   

}
