#pragma once


#include <mod/gcode/Command.hpp>

namespace mod {
namespace gcode {

class CommandFactory {
private:
    static const CommandFactory * _instance;
    static const CommandPrimitive _knownCommands[];
    static const size_t _nCommands;
    
    void _parseArgs(Command& cmd) const;
    const CommandPrimitive * _find(const char * cmd) const;
    
public:
    
    static const CommandFactory * getInstance();
    bool parse(Command& cmd) const;
};

}  // namespace gcode
}  // namespace mod
