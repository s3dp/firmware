#pragma once

#include <cstdint>
#include <hal/serial.hpp>
#include <functional>

namespace mod{
namespace gcode{

struct Command;
    
struct CommandPrimitive{
    using handler_t = std::function<void(const Command&)>;
    
    /**
     * Command set (M, G, etc)
     * 
     * @example: set will be 'M' for 'M117' command
     */
    char set;
    
    /**
     * Commmand index (1, 2, 3, etc)
     * 
     * @example: index will be 117 for 'M117' command
     */
    uint16_t index;
    
    handler_t handler;
    
};    

struct Command: CommandPrimitive{
    
    char rawCommand[256]; 
    
    
    /**
     * Array of command arguments
     * 
     * @example: argv will be ["M117", "S0"] for "M117 S0" command
     * 
     */
    char * argv[10];
    
    /**
     * Number of argumments in argv
     */
    size_t argc;
    
    /**
     * Source serial port.
     * It should be used to response.
     */
    hal::Serial * source;
    
    
    
};
}
    
}
