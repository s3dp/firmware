#pragma once


#include <cstdint>
#include <helpers/Initable.hpp>


namespace mod {
namespace thermal {
    
    


/**
 * The temperature is stored and measured in 1/100 degrees Celsius with an exception of -30000
 * (-300 deg. Celsius's.) Temperature value is int32_t (signed, units is 1/100 degrees Celsius)
 * One heater and one thermometer are assigned to each Regulator object.
 */
class Regulator: public helpers::Initable{
    
public:
    
    static const int32_t TEMP_DISABLE = -30000;
    
    virtual void setTemperature(int32_t temp) = 0; 
    virtual int32_t getMeasuredTemperature() const = 0;
    virtual int32_t getTargedTemperature() const = 0;
    
    /**
     * do some regulator work
     * 
     * @param dt is time delta since last call to this function (unit is millisecond)
     */
    virtual void tick(uint32_t dt) = 0;
    
    virtual ~Regulator() = default;
    
}; // class Regulator

} // namespace thermal
} // namespace mod
