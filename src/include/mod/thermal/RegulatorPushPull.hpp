#pragma once

#include <dev/thermal/Heater.hpp>
#include <dev/thermal/Sensor.hpp>
#include <mod/thermal/Regulator.hpp>

namespace mod {
namespace thermal {

class RegulatorPushPull : public Regulator {
protected:
    using Heater = dev::thermal::Heater;
    using Sensor = dev::thermal::Sensor;
    /**
     * Hysteresis
     */
    const int32_t _HYST = 200;  // 2 degrees Celsius
    Sensor * _sensor;
    Heater * _heater;
    bool _heater_state;
    int32_t _target_temp;

public:
    RegulatorPushPull(const Sensor * sensor, const Heater * heater);

    virtual void setTemperature(int32_t temp) override;
    virtual int32_t getMeasuredTemperature() const override;
    virtual int32_t getTargedTemperature() const override;

    virtual void tick(uint32_t dt) override;

    virtual ~RegulatorPushPull() = default;
protected:
    virtual void _init() override;
};  // class RegulatorPushPull

}  // namespace thermal
}  // namespace mod
