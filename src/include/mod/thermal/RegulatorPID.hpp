#pragma once

#include <dev/thermal/Heater.hpp>
#include <dev/thermal/Sensor.hpp>
#include <mod/thermal/Regulator.hpp>

namespace mod {
namespace thermal {

class RegulatorPID : public Regulator {
public:
    using Heater = dev::thermal::Heater;
    using Sensor = dev::thermal::Sensor;
    struct Config{
        Sensor * sensor;
        Heater * heater;
        const int32_t kP, kD, kI;
    };

private:
    static const int16_t PID_MAX_DELTA = 1000; //< 10 deg. Celsius
    const Config * _cfg;
    int32_t _target_temp;
    int32_t _tempPrev;
    int32_t vP, vD, vI;    

public:
    RegulatorPID(const Config * cfg);
    virtual void setTemperature(int32_t temp) override;
    virtual int32_t getMeasuredTemperature() const override;
    virtual int32_t getTargedTemperature() const override;
    virtual void tick(uint32_t dt) override;

    virtual ~RegulatorPID() = default;

protected:
    virtual void _init() override;

};  // class RegulatorPID

}  // namespace thermal
}  // namespace mod
