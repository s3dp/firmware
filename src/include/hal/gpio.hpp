#pragma once

#include <cstdint>
#include <helpers/Initable.hpp>

namespace hal {
    
enum GPIOMode : uint8_t {
    MODE_ANAL = 0,
    MODE_OUT  = 1,
    MODE_IN   = 2,
    MODE_AF   = 3
};

enum eGPIO : uint32_t {
    PA0 =  (0<<(16))|1<<0,
    PA1 =  (0<<(16))|1<<1,
    PA2 =  (0<<(16))|1<<2,
    PA3 =  (0<<(16))|1<<3,
    PA4 =  (0<<(16))|1<<4,
    PA5 =  (0<<(16))|1<<5,
    PA6 =  (0<<(16))|1<<6,
    PA7 =  (0<<(16))|1<<7,
    PA8 =  (0<<(16))|1<<8,
    PA9 =  (0<<(16))|1<<9,
    PA10 = (0<<(16))|1<<10,
    PA11 = (0<<(16))|1<<11,
    PA12 = (0<<(16))|1<<12,
    PA13 = (0<<(16))|1<<13,
    PA14 = (0<<(16))|1<<14,
    PA15 = (0<<(16))|1<<15,
    
    
    PB0  = (1<<(16))|1<<0,
    PB1  = (1<<(16))|1<<1,
    PB2  = (1<<(16))|1<<2,
    PB3  = (1<<(16))|1<<3,
    PB4  = (1<<(16))|1<<4,
    PB5  = (1<<(16))|1<<5,
    PB6  = (1<<(16))|1<<6,
    PB7  = (1<<(16))|1<<7,
    PB8  = (1<<(16))|1<<8,
    PB9  = (1<<(16))|1<<9,
    PB10 = (1<<(16))|1<<10,
    PB11 = (1<<(16))|1<<11,
    PB12 = (1<<(16))|1<<12,
    PB13 = (1<<(16))|1<<13,
    PB14 = (1<<(16))|1<<14,
    PB15 = (1<<(16))|1<<15,
    
    PC0  = (2<<(16))|1<<0,
    PC1  = (2<<(16))|1<<1,
    PC2  = (2<<(16))|1<<2,
    PC3  = (2<<(16))|1<<3,
    PC4  = (2<<(16))|1<<4,
    PC5  = (2<<(16))|1<<5,
    PC6  = (2<<(16))|1<<6,
    PC7  = (2<<(16))|1<<7,
    PC8  = (2<<(16))|1<<8,
    PC9  = (2<<(16))|1<<9,
    PC10 = (2<<(16))|1<<10,
    PC11 = (2<<(16))|1<<11,
    PC12 = (2<<(16))|1<<12,
    PC13 = (2<<(16))|1<<13,
    PC14 = (2<<(16))|1<<14,
    PC15 = (2<<(16))|1<<15,
    
    PD0  = (3<<(16))|1<<0,
    PD1  = (3<<(16))|1<<1,
    PD2  = (3<<(16))|1<<2,
    PD3  = (3<<(16))|1<<3,
    PD4  = (3<<(16))|1<<4,
    PD5  = (3<<(16))|1<<5,
    PD6  = (3<<(16))|1<<6,
    PD7  = (3<<(16))|1<<7,
    PD8  = (3<<(16))|1<<8,
    PD9  = (3<<(16))|1<<9,
    PD10 = (3<<(16))|1<<10,
    PD11 = (3<<(16))|1<<11,
    PD12 = (3<<(16))|1<<12,
    PD13 = (3<<(16))|1<<13,
    PD14 = (3<<(16))|1<<14,
    PD15 = (3<<(16))|1<<15,
    
    PE0  = (4<<(16))|1<<0,
    PE1  = (4<<(16))|1<<1,
    PE2  = (4<<(16))|1<<2,
    PE3  = (4<<(16))|1<<3,
    PE4  = (4<<(16))|1<<4,
    PE5  = (4<<(16))|1<<5,
    PE6  = (4<<(16))|1<<6,
    PE7  = (4<<(16))|1<<7,
    PE8  = (4<<(16))|1<<8,
    PE9  = (4<<(16))|1<<9,
    PE10 = (4<<(16))|1<<10,
    PE11 = (4<<(16))|1<<11,
    PE12 = (4<<(16))|1<<12,
    PE13 = (4<<(16))|1<<13,
    PE14 = (4<<(16))|1<<14,
    PE15 = (4<<(16))|1<<15,
    
    PF0  = (5<<(16))|1<<0,
    PF1  = (5<<(16))|1<<1,
    PF2  = (5<<(16))|1<<2,
    PF3  = (5<<(16))|1<<3,
    PF4  = (5<<(16))|1<<4,
    PF5  = (5<<(16))|1<<5,
    PF6  = (5<<(16))|1<<6,
    PF7  = (5<<(16))|1<<7,
    PF8  = (5<<(16))|1<<8,
    PF9  = (5<<(16))|1<<9,
    PF10 = (5<<(16))|1<<10,
    PF11 = (5<<(16))|1<<11,
    PF12 = (5<<(16))|1<<12,
    PF13 = (5<<(16))|1<<13,
    PF14 = (5<<(16))|1<<14,
    PF15 = (5<<(16))|1<<15,
    
    PG0  = (6<<(16))|1<<0,
    PG1  = (6<<(16))|1<<1,
    PG2  = (6<<(16))|1<<2,
    PG3  = (6<<(16))|1<<3,
    PG4  = (6<<(16))|1<<4,
    PG5  = (6<<(16))|1<<5,
    PG6  = (6<<(16))|1<<6,
    PG7  = (6<<(16))|1<<7,
    PG8  = (6<<(16))|1<<8,
    PG9  = (6<<(16))|1<<9,
    PG10 = (6<<(16))|1<<10,
    PG11 = (6<<(16))|1<<11,
    PG12 = (6<<(16))|1<<12,
    PG13 = (6<<(16))|1<<13,
    PG14 = (6<<(16))|1<<14,
    PG15 = (6<<(16))|1<<15
    
};

class GPIO : public helpers::Initable {
public:
    GPIO(const eGPIO portpin, const GPIOMode mode = GPIOMode::MODE_OUT, const bool doInit = true);
//     GPIO(const uint32_t port, const uint16_t pin);
    
    void toggle() const;
    void write(bool value) const;
    void set() const;
    void reset() const;
    bool read() const;
    
private:
    const uint32_t _port;
    const uint16_t _pin;
    const GPIOMode _mode;
    static const uint32_t _PortBinds[];
    
public:
    virtual void _init();
};


}
