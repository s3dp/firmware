#pragma once

#include <cstddef>
#include <helpers/Initable.hpp>
#include <functional>
#include <hal/serial.hpp>

struct _usbd_device;
namespace hal{
class USB: 
    public virtual helpers::Initable,
    public virtual hal::Serial
{
public:
    static const size_t BUFFER_SIZE = 128;
    
    USB(bool autoinit = false);
    static USB * getInstance();
    virtual ~USB();
    
    
    
    virtual size_t read(void * where, size_t max_count) override;
    
    virtual size_t write(const void * what, size_t count) override;
    
    virtual uint8_t poll(const uint8_t what) override;
    
    friend void cdcacm_data_rx_cb(_usbd_device *usbd_dev, uint8_t ep);
    virtual void flush() override;
    
private:
    
    static USB * _instance;
    static uint8_t _buf[];
    static size_t _bStart, _bLen;
    virtual void _init();
    
};

void cdcacm_data_rx_cb(_usbd_device *usbd_dev, uint8_t ep);





}
