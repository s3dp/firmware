#pragma once

#include <helpers/Initable.hpp>
#include <hal/gpio.hpp>
#include <cstdint>

namespace hal{
class ADC: public helpers::Initable {
public:
    enum : uint16_t{
        DEFAULT_VREF = 3300
    };
    
    /**
     * Creates ADC channel;
     * @param device - index of ADC (ADC1, ADC2, etc)
     * @param channel - ADC channel
     * @param vRef - reference voltage of this ADC (1 is equivalent to 1 mV)
     * @param doInit - true if you want to initialize it without "force" flag
     * 
     */
    
    ADC(const GPIO * pin, uint8_t device, uint8_t channel, uint16_t vRef = DEFAULT_VREF, bool doInit = false);
    
    /**
     * Returns voltage
     * @return voltage (1 is equivalent to 1 mV)
     */
    uint16_t getMeasuredVoltage() const;
    
    /**
     * Returns measured value
     * @return value between 0 and 65535
     */
    uint16_t getMeasuredValue() const;
    
    virtual ~ADC() = default;
protected:
    void _init();
private:
    const uint16_t _vRef;
    const uint16_t _channel;
    const uint16_t _dev;
    const GPIO * _pin;
    uint16_t _measure() const;
    uint16_t _convert(uint16_t what) const;
};
}
