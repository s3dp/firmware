#pragma once

#include <functional>
#include <helpers/Initable.hpp>

namespace hal{
class Serial: virtual public helpers::Initable{
public:
    Serial() = default;
    virtual ~Serial() = default;
    enum PollStatus: uint8_t {
        STATUS_NONE = 0,
        STATUS_IDATA = 1,
    };
    virtual size_t read(void * where, size_t max_count) = 0;
    virtual size_t write(const void * what, size_t count) = 0;
    virtual uint8_t poll(const uint8_t what) = 0;
    virtual void flush() = 0;
};
}
