#pragma once


#include <stdint.h>
#include <helpers/cheader.h>

__CHEADER_BEGIN
    
void clock_use_preset(uint32_t n);
    
__CHEADER_END
