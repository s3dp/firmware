#pragma once

#include <boards/board.hpp>
#include <dev/stepper/Stepper.hpp>
#include <hal/serial.hpp>
#include <helpers/Initable.hpp>
#include <mod/thermal/Regulator.hpp>

namespace printer {

class Printer : virtual public helpers::Initable {
public:
    using stepper_t   = dev::stepper::Stepper;
    using board_t     = boards::Board;
    using serial_t    = hal::Serial;
    using regulator_t = mod::thermal::Regulator;

    struct PrintConf {
        const board_t * board;
        const stepper_t ** steppers;
        const serial_t ** serials;
        const regulator_t ** hotends;
        const regulator_t * table;
    };

    using init_t = PrintConf;
    Printer(const init_t * _modules);
    virtual ~Printer() = default;
    
    const PrintConf * modules;
    static Printer * getInstance();

protected:
    virtual void _init();

private:
    static const Printer * _instance;
};

}  // namespace printer
