#pragma once

#include <cstdint>
#include <functional>

namespace freertos {
using task_fn_t = void (*)(void *);
void * taskCreate(task_fn_t fn, const char * name);
void taskSuspend(void * pid);
void taskResume(void * pid);
void taskKill(void * pid);

void taskDelayUntil(uint32_t& last_ms, uint32_t ms);
void taskDelay(uint32_t ms);
uint32_t time();
}  // namespace freertos
