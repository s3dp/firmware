#pragma once

#include <vector>
#include <helpers/Initable.hpp>
#include <hal/gpio.hpp>
#include <dev/stepper/PinInterface.hpp>



#include <cstddef>



namespace boards {
class Board: public helpers::Initable {
    
public:
    
    
    struct Initializer{
        const helpers::Initable** initables;
        const size_t initables_size;
        const hal::GPIO* led;
    };
    using initializer_t = Initializer;
    Board(const initializer_t* initializer);
    virtual ~Board();
    
    const hal::GPIO * led() const;
    
    bool sanityCheck();
protected:
    virtual void _init();
    const Initializer * _initializer;
    
};


extern const Board instance;

}
