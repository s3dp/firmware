#pragma once

#include <dev/thermal/Sensor.hpp>

#include <hal/adc.hpp>

namespace dev {
namespace thermal {
    
struct LinearSensorPreset{
    
    /**
     * simplified Steinhart-Hart eauation's T0
     * units is 1/100 degrees Celsius
     */
    uint32_t temperature_initial;
    
    /**
     * simplified Steinhart-Hart eauation's R0
     */
    uint32_t resistance_initial;
    
    /**
     * simplified Steinhart-Hart eauation's B
     */
    uint32_t b_value;
    uint32_t pullup_resistance = 4700;
};

extern LinearSensorPreset NTC100K_4K7_PULLUP;
extern LinearSensorPreset NTC100K_1K_PULLUP;

class SensorAnalogLinear: public Sensor {
private:
    const hal::ADC * _adc;
    LinearSensorPreset * _preset;
public:
    SensorAnalogLinear(hal::ADC * adc, LinearSensorPreset * preset);
    
    virtual int32_t measure() override;
    
    virtual ~SensorAnalogLinear();
    
protected:
    virtual void _init() override;
};



} // namespace thermal
} // namespace dev
