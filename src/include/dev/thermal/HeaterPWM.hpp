#pragma once

#include <dev/thermal/Heater.hpp>

namespace dev {
namespace thermal {

class HeaterPWM : public Heater {
    virtual void setPower(uint16_t power) override;
    virtual uint16_t getPower() const override;

    /**
     * It should disable heaters
     */
    virtual ~HeaterPWM();
};

}  // namespace thermal
}  // namespace dev
