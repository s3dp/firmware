#pragma once


#include <hal/gpio.hpp>
#include <dev/thermal/Heater.hpp>


namespace dev {
namespace thermal {

/**
 * Heater which works in on/off mode only 
 * (max power or no power)
 * 
 * zero power means disable,
 * any non-zero power means enable
 */
class HeaterPushPull: public Heater {
private:
    const hal::GPIO * _pin;
    bool _state;
public:
    HeaterPushPull(hal::GPIO* pin);
    
    virtual void setPower(uint16_t power) override;
    virtual uint16_t getPower() const override;
    
    /**
     * It should disable heaters
     */
    virtual ~HeaterPushPull();
protected:
    virtual void _init() override;
};




} // namespace thermal
} // namespace dev
