#pragma once

#include <cstdint>
#include <helpers/Initable.hpp>

namespace dev {
namespace thermal {

/**
 * The temperature is stored and measured in 1/100 degrees Celsius with an exception of -30000
 * (-300 deg. Celsius's.) Temperature value is int32_t (signed, units is 1/100 degrees Celsius)
 */
class Sensor : public helpers::Initable {
public:
    virtual int32_t measure() = 0;
    virtual ~Sensor()         = default;
};

}  // namespace thermal
}  // namespace dev
