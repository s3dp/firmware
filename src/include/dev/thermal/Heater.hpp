#pragma once

#include <cstdint>
#include <helpers/Initable.hpp>

namespace dev {
namespace thermal {

/**
 * 0 is equivalent to disable
 * 0xffff is equivalent to maximal power (always on)
 */
class Heater : public helpers::Initable {
public:
    static const uint16_t PWR_MAX = UINT16_MAX;
    static const uint16_t PWR_NONE = 0;
    virtual void setPower(uint16_t power) = 0;
    virtual uint16_t getPower() const     = 0;
    virtual ~Heater()                     = default;
};

}  // namespace thermal
}  // namespace dev
