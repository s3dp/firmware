#pragma once

#include <cstddef>

namespace dev::serial{
class Serial{
public:
    Serial() = default;
    virtual ~Serial() = default;
   
    virtual void write(const char * what, size_t len) const = 0;
    virtual size_t read(const char * where, size_t max_len) const = 0;
};
}
