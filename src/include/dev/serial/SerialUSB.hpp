#pragma once

#include <dev/serial/Serial.hpp>
#include <helpers/Initable.hpp>


namespace dev::serial{
class SerialUSB: public Serial , public helpers::Initable{
public:
    SerialUSB();
    virtual ~SerialUSB();
    
    virtual void _init();
    
};
}
