#pragma once
#include <hal/gpio.hpp>
#include <helpers/Initable.hpp>

namespace dev {
namespace stepper {
class PinInterface: public helpers::Initable {
public:    
    PinInterface(const hal::GPIO * en, const hal::GPIO * step, const hal::GPIO * dir, const bool doInit = true);
    virtual ~PinInterface();
    const hal::GPIO* _en;
    const hal::GPIO* _step;
    const hal::GPIO* _dir;
    
    
protected:
    void _init() override;
};
}
}
