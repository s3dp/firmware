#pragma once
#include <dev/stepper/Stepper.hpp>
#include <hal/gpio.hpp>
#include <dev/stepper/PinInterface.hpp>

namespace dev::stepper{
class A4988: public Stepper {
public:
    A4988(const PinInterface * iFace);
    virtual void step() override;
    virtual void dir(bool d) override;
    virtual void enable(bool e) override;
protected:
    const PinInterface * _iFace;
};
}
