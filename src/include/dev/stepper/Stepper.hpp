#pragma once
#include <helpers/Initable.hpp>


namespace dev{
namespace stepper{
class Stepper: public helpers::Initable{
public:
    Stepper() = default;
    virtual void step() = 0;
    virtual void dir(bool) = 0;
    virtual void enable(bool) = 0;
};

}
}
