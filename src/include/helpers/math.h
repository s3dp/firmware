#pragma once

#include <cstdint>

namespace helpers {
template<typename T>
inline bool inBounds(const T& value, const T& min, const T& max) {
    return (value >= min) && (value <= max);
}

template<typename T>
inline T toBounds(const T& value, const T& min, const T& max) {
    return ((value>=min)?(value<=max?value:max):min);
}


}
