#pragma once




namespace helpers{
class Initable {
public:
    Initable(bool required = true);
    
    void init(bool force = false) const;
    
    virtual ~Initable() = default;
private:
    volatile bool _required;
    volatile bool _inited;
protected:
    virtual void _init() = 0;
    
};
}
