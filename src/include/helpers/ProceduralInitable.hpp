#pragma once

#include <helpers/Initable.hpp>
#include <functional>


namespace helpers{
    
    
class ProceduralInitable: public Initable{
public:
    using fn_t = std::function<void(void)>;
    ProceduralInitable(const fn_t& fn, bool force);
    virtual ~ProceduralInitable() = default;
    
    
protected:
    fn_t _fn;
    virtual void _init() override;
};

}
