# Prepare

Install software: arm-none-eabi-gcc, arm-none-eabi-gdb, arm-none-eabi-newlib, cmake, 
make, git, python3, stlink

# Build

Clone this project:

```
git clone <url>
```

Go to project path.

Download project dependencies:

```
git submodule update --init
```

Build libopencm3 (You can't proceed without this step because we use it in CMake configuration step).

```
make -C 3dparty/libopencm3
```

Create 
```
build
```
directory and go to it.

```
mkdir build
cd build
```

Generate build files (we prefer ninja, but you can use make instead):

```
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Debug
```

Build:

```
ninja
```

# Flash
You can use ninja task:

```
ninja flash
```

Also you can use st-flash directly (make sure that you are in the build directory):

```
st-flash write src/app/firmware.bin 0x08000000
```

# Debug

You can use gdb or openocd for on-chip debugging. We will explain only gdb way.

Start st-util for remote debugging (don't be confused because it showld work through all debug process):

```
st-util -m
```

Open another terminal and run gdb (in build directory):

```
arm-none-eabi-gdb src/app/firmware.elf
```

In GDB you should connect to st-util interface:

```
tar rem :4242
```

Next you can use GDB as you wish.

To reset MCU use next command:

```
mon reset halt
```
